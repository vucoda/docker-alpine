FROM scratch
LABEL maintainer="Vu Han Mach <vucoda@gmail.com>" 

ARG BUILD_DATE
ARG BUILD_TAG

LABEL org.label-schema.schema-version="1.0" \
      org.label-schema.name="foswiki" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.description="Built on the alpine image with perl, fast-cgi and nginx." \
      org.label-schema.version="0.1-dev" \
      org.label-schema.docker.cmd="docker run -d --name foswiki -p 80:80 --mount source=/path/to/vol,destination=/var/www/foswiki,type=bind $BUILD_TAG" \
      org.label-schema.docker.cmd.devel="docker run -ti --rm --network foswiki-nw -p 80:80 --mount source=$(pwd)/tmp,destination=/var/www/foswiki,type=bind $BUILD_TAG"

ENV ALPINE_ARCH x86_64
ENV ALPINE_VERSION 3.10.1

ADD alpine-minirootfs-${ALPINE_VERSION}-${ALPINE_ARCH}.tar.gz /
CMD ["/bin/sh"]
